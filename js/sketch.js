/*
 *   SQUAT Challenge
 *   
 *   olivier.nocent@univ-reims.fr
 */

let capture;
let captureReady = false;
let captureScaleFactor, captureOffsetX;

let pose = null;
let poseReady = false;

function setup() {
    let canvas = createCanvas(windowWidth, windowHeight);
    canvas.parent("canvas");

    let constraints = {
        video: {
            facingMode: "environment"
        },
        audio: false
    };
    capture = createCapture(constraints, () => {
        captureReady = true;
        captureScaleFactor = height / capture.height;
        captureOffsetX = (width - capture.width * captureScaleFactor) / 2;

        // Create a new poseNet method
        let options = {
            detectionType: 'single',
            minConfidence: 0.8
        }
        const poseNet = ml5.poseNet(capture, options, modelLoaded);

        // Listen to new 'pose' events
        poseNet.on('pose', (results) => {
            pose = results[0].pose;
            poseReady = true;
        });
    });
    capture.hide();
}

function draw() {
    background(0);
    if (captureReady) {
        push();
        translate(captureOffsetX, 0);
        scale(captureScaleFactor, captureScaleFactor);
        image(capture, 0, 0);

        if (poseReady) {
            drawSkeleton(pose);
        }
        pop();
    }
}

function drawSkeleton(pose) {
    strokeWeight(4);
    stroke(255, 255, 0)
    line(pose.leftShoulder.x, pose.leftShoulder.y, pose.rightShoulder.x, pose.rightShoulder.y);
    line(pose.leftShoulder.x, pose.leftShoulder.y, pose.leftHip.x, pose.leftHip.y);
    line(pose.rightShoulder.x, pose.rightShoulder.y, pose.rightHip.x, pose.rightHip.y);
    line(pose.leftHip.x, pose.leftHip.y, pose.rightHip.x, pose.rightHip.y);
    line(pose.leftShoulder.x, pose.leftShoulder.y, pose.leftElbow.x, pose.leftElbow.y);
    line(pose.leftElbow.x, pose.leftElbow.y, pose.leftWrist.x, pose.leftWrist.y);
    line(pose.rightShoulder.x, pose.rightShoulder.y, pose.rightElbow.x, pose.rightElbow.y);
    line(pose.rightElbow.x, pose.rightElbow.y, pose.rightWrist.x, pose.rightWrist.y);
    stroke(255, pose.leftKnee.confidence * 255, 0);
    line(pose.leftHip.x, pose.leftHip.y, pose.leftKnee.x, pose.leftKnee.y);
    line(pose.leftKnee.x, pose.leftKnee.y, pose.leftAnkle.x, pose.leftAnkle.y);
    stroke(255, pose.rightKnee.confidence * 255, 0);
    line(pose.rightHip.x, pose.rightHip.y, pose.rightKnee.x, pose.rightKnee.y);
    line(pose.rightKnee.x, pose.rightKnee.y, pose.rightAnkle.x, pose.rightAnkle.y);
    stroke(0);
    fill(255);
    circle(pose.leftShoulder.x, pose.leftShoulder.y, 10);
    circle(pose.rightShoulder.x, pose.rightShoulder.y, 10);
    circle(pose.leftHip.x, pose.leftHip.y, 10);
    circle(pose.rightHip.x, pose.rightHip.y, 10);
    circle(pose.leftElbow.x, pose.leftElbow.y, 10);
    circle(pose.leftWrist.x, pose.leftWrist.y, 10);
    circle(pose.rightElbow.x, pose.rightElbow.y, 10);
    circle(pose.rightWrist.x, pose.rightWrist.y, 10);
    circle(pose.leftKnee.x, pose.leftKnee.y, 10);
    circle(pose.leftAnkle.x, pose.leftAnkle.y, 10);
    circle(pose.rightKnee.x, pose.rightKnee.y, 10);
    circle(pose.rightAnkle.x, pose.rightAnkle.y, 10);
}

// When the model is loaded
function modelLoaded() {
    console.log('Model Loaded!');
}

/*
Pose attributes:
    leftHip: Object { x: , y: , confidence:  }
    ​​leftEar: ...
    ​​leftElbow: ...
    ​​leftEye: ...
    ​​leftHip: ...
    ​​leftKnee: ...
    ​​leftShoulder: ...
    ​​leftWrist: ...
    ​​nose: ...
    ​​rightHip: ...
    ​​rightEar: ...
    ​​rightElbow: ...
    ​​rightEye: ...
    ​​rightHip: ...
    ​​rightKnee: ...
    ​​rightShoulder: ...
    ​​rightWrist: ...
*/